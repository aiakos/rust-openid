#[derive(Deserialize, Debug)]
pub struct JWK {
    kty: i32,
    #[serde(rename = "use")]
    usage: i32,
    key_ops: i32,
    alg: i32,
    kid: i32,
}

#[derive(Deserialize, Debug)]
pub struct Certs {
    keys: Vec<JWK>
}
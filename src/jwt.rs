use std::string::FromUtf8Error;

use base64::{decode_config, Base64Error, URL_SAFE_NO_PAD};
use ring::digest;
use ring::hmac;
use ring::signature;
use untrusted::Input;
use serde::Deserialize;
use serde_json::{from_str, Error as JSONError};


#[derive(Debug)]
pub enum Error {
    Base64,
    UTF8,
    JSON,
    Signature,
    Length
}

#[derive(Deserialize, Debug)]
enum Encoding {
    HS256,
    RS256,
    ES256,
    None,
}

#[derive(Deserialize, Debug)]
pub struct Header {
    alg: Encoding,
    typ: String,
}

pub trait Claims {
    fn decode(input: &str) -> Result<Self, Error> where Self: Sized;
}

pub fn parse<T: Claims>(jwt: &str, secret: &str) -> Result<(Header, T), Error> {
    let mut iter = jwt.split(".");

    let header = iter.next().ok_or(Error::Length)?;
    let claims = iter.next().ok_or(Error::Length)?;
    let signature = iter.next().ok_or(Error::Length)?;
    let signature = decode_config(signature, URL_SAFE_NO_PAD)?;
    let message = header.to_owned() + "." + claims;

    let header = parse_header(header)?;
    println!("Header ok");
    let claims = parse_claims(claims)?;
    verify_signature(&header.alg, message.as_bytes(), secret.as_bytes(), signature.as_slice())?;

    Ok((header, claims))
}

fn parse_header(header: &str) -> Result<Header, Error> {
    let decoded = base64_to_string(header)?;

    Ok(from_str(&decoded)?)
}

fn parse_claims<T>(claims: &str) -> Result<T, Error> where T: Claims {
    let decoded = base64_to_string(claims)?;

    T::decode(&decoded)
}

fn base64_to_string(header: &str) -> Result<String, Error> {
    let decoded = decode_config(header, URL_SAFE_NO_PAD)?;

    Ok(String::from_utf8(decoded)?)
}

fn verify_signature(method: &Encoding, message: &[u8], secret: &[u8], signature: &[u8]) -> Result<(), Error> {
    match method {
        &Encoding::None => match signature.is_empty() {
            true => Ok(()),
            false => Err(Error::Signature)
        },
        &Encoding::HS256 => verify_hmac(message, secret, signature),
        &Encoding::RS256 => verify_asymmetric(message, secret, signature, &signature::RSA_PKCS1_2048_8192_SHA256),
        &Encoding::ES256 => verify_asymmetric(message, secret, signature, &signature::ECDSA_P256_SHA256_ASN1)
    }
}

fn verify_asymmetric<T: signature::VerificationAlgorithm + 'static>(message: &[u8], secret: &[u8], signature: &[u8], method: &T) -> Result<(), Error> {
    let message = Input::from(message);
    let secret = Input::from(secret);
    let signature = Input::from(signature);

    signature::verify(method, secret, message, signature).map_err(|_| Error::Signature)
}

fn verify_hmac(message: &[u8], secret: &[u8], signature: &[u8]) -> Result<(), Error> {
    let key = hmac::VerificationKey::new(&digest::SHA256, &secret);
    hmac::verify(&key, message, signature).map_err(|_| Error::Signature)
}

impl From<Base64Error> for Error {
    fn from(_: Base64Error) -> Error {
        Error::Base64
    }
}

impl From<FromUtf8Error> for Error {
    fn from(_: FromUtf8Error) -> Error {
        Error::UTF8
    }
}

impl<T> Claims for T where T: Deserialize {
    fn decode(input: &str) -> Result<Self, Error> {
        Ok(from_str(input)?)
    }
}

impl From<JSONError> for Error {
    fn from(_: JSONError) -> Error {
        Error::JSON
    }
}
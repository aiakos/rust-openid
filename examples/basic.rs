#[macro_use]
extern crate serde_derive;
extern crate jwk;

use jwk::jwt::parse;

#[derive(Deserialize, Debug)]
struct Claims {
    sub: String,
    exp: i32
}

fn main() {
    let jwt = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE0ODgzMTc0MjAsImV4cCI6MTUxOTg1MzQyMCwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSIsIkdpdmVuTmFtZSI6IkpvaG5ueSIsIlN1cm5hbWUiOiJSb2NrZXQiLCJFbWFpbCI6Impyb2NrZXRAZXhhbXBsZS5jb20iLCJSb2xlIjpbIk1hbmFnZXIiLCJQcm9qZWN0IEFkbWluaXN0cmF0b3IiXX0.cSAJX0iHsj2UBuN9UOhj8-KSrYdJScgXC-qAXq9Kp3g";

    let (header, claims) = parse::<Claims>(jwt, "sec").unwrap();

    println!("{:?}", header);
    println!("{:?}", claims);
}